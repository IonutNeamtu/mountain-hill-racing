//
//  CarClass.swift
//  Hill Racing
//
//  Created by Neamțu Ionuț-Roberto on 5/10/18.
//  Copyright © 2018 Neamțu Ionuț-Roberto. All rights reserved.
//

import Foundation
import SpriteKit

class CarClass{
    
    let carBodyFrame = SKSpriteNode(imageNamed: "carFrame.png")
    let wheelFront = SKSpriteNode(imageNamed: "wheel1.png")
    let wheelRear = SKSpriteNode(imageNamed: "wheel1.png")
    
    let pinFront = SKSpriteNode(color: UIColor.black, size: CGSize(width: 1, height: 70))
    let pinRear = SKSpriteNode(color: UIColor.black, size: CGSize(width: 1, height: 70))
    
    let carCategory: UInt32 = 0x1
    
    init(suspensionHeight: CGFloat) {
        
        carBodyFrame.position = CGPoint(x: 250, y: 400)
        let path = CGMutablePath()
        //path car shape physicBody
        path.addLines(between: [CGPoint(x: -carBodyFrame.size.width/2, y: carBodyFrame.size.height * 0.013),
                                CGPoint(x: -carBodyFrame.size.width/2, y: carBodyFrame.size.height * 0.12),
                                CGPoint(x: -carBodyFrame.size.width * 0.05, y: carBodyFrame.size.height * 0.12),
                                CGPoint(x: -carBodyFrame.size.width * 0.034, y: carBodyFrame.size.height/2 ),
                                CGPoint(x: carBodyFrame.size.width * 0.166, y: carBodyFrame.size.height/2 ),
                                CGPoint(x: carBodyFrame.size.width * 0.21, y: carBodyFrame.size.height * 0.323),
                                CGPoint(x: carBodyFrame.size.width * 0.416, y: carBodyFrame.size.height * 0.252),
                                CGPoint(x: carBodyFrame.size.width/2, y: -carBodyFrame.size.height * 0.013),
                                CGPoint(x: carBodyFrame.size.width * 0.266, y: -carBodyFrame.size.height  * 0.013),
                                CGPoint(x: carBodyFrame.size.width * 0.233, y: -carBodyFrame.size.height * 0.412),
                                CGPoint(x: -carBodyFrame.size.width * 0.167, y: -carBodyFrame.size.height * 0.412),
                                CGPoint(x: -carBodyFrame.size.width * 0.184, y: -carBodyFrame.size.height * 0.058),
                                CGPoint(x: -carBodyFrame.size.width/2, y: carBodyFrame.size.height * 0.013)])
        path.closeSubpath()
        
        
        carBodyFrame.physicsBody = SKPhysicsBody(polygonFrom: path)
        carBodyFrame.physicsBody?.mass *= 1.4
        
        
        // Pin Front wheel
        pinFront.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 7, height: 0.9 * carBodyFrame.size.width/3))
        pinFront.position = CGPoint(x: carBodyFrame.position.x + 0.4 * carBodyFrame.size.width, y: (carBodyFrame.position.y - carBodyFrame.size.height/2) - suspensionHeight * carBodyFrame.size.height)
        
        //Pin Rear wheel
        pinRear.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 7, height: 0.9 * carBodyFrame.size.width/3))
        pinRear.position = CGPoint(x: carBodyFrame.position.x - 0.34 * carBodyFrame.size.width, y: (carBodyFrame.position.y - carBodyFrame.size.height/2) - suspensionHeight * carBodyFrame.size.height)
        
        
        //Front Wheel
        //wheelFront.scale(to: CGSize(width: carSize/3, height: carSize/3))
        wheelFront.physicsBody = SKPhysicsBody(circleOfRadius: wheelFront.size.width/2)
        wheelFront.position = pinFront.position
        
        //Rear Wheel
        //wheelRear.scale(to: CGSize(width: carSize/3, height: carSize/3))
        wheelRear.physicsBody = SKPhysicsBody(circleOfRadius: wheelRear.size.width/2)
        wheelRear.position = pinRear.position
        
        //wheels are rendered over the carBodyFrame
        carBodyFrame.zPosition = 1
        wheelFront.zPosition = 2
        wheelRear.zPosition = 2
        
        wheelRear.physicsBody?.friction = 5
        wheelFront.physicsBody?.friction = 5
        
        wheelRear.physicsBody?.restitution = 0.1
        wheelFront.physicsBody?.restitution = 0.1;
    }
}
