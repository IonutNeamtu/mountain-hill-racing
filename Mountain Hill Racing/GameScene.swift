//
//  GameScene.swift
//  Hill Racing
//
//  Created by Neamțu Ionuț-Roberto on 5/10/18.
//  Copyright © 2018 Neamțu Ionuț-Roberto. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var location = CGPoint.zero
    var touched:Bool = false
    let myCar = CarClass(suspensionHeight: 0.4)
    var borderXPosition: CGFloat = 0.0;
    var borderYPosition: CGFloat = 0.0;
    var border = SKSpriteNode(color: UIColor.red, size: CGSize(width: 10, height: 1000));
    var myCamera: SKCameraNode?
    var moveForward = false
    var myCount = 0
    let groundTexture = SKTexture(imageNamed: "fly_ground.png")
    let groundTexture0 = SKTexture(imageNamed: "fly0.png")
    let groundTexture1 = SKTexture(imageNamed: "fly1.png")
    let groundTexture2 = SKTexture(imageNamed: "fly2.png")
    let groundTexture3 = SKTexture(imageNamed: "fly3.png")
    let groundTexture4 = SKTexture(imageNamed: "fly4.png")
    let groundTexture5 = SKTexture(imageNamed: "fly5.png")
    let groundTexture6 = SKTexture(imageNamed: "fly6.png")
    let groundTexture7 = SKTexture(imageNamed: "fly7.png")
    let groundTexture8 = SKTexture(imageNamed: "fly8.png")
    var isOnGround: Bool = false
    let groundCategory: UInt32 = 0x1 << 1
    var groundXY: [[CGPoint]] = [[CGPoint(x: 0, y: 0),
                                  CGPoint(x: 100, y: 50),
                                  CGPoint(x: 200, y: 50),
                                  CGPoint(x: 300, y: 10),
                                  CGPoint(x: 400, y: 20),
                                  CGPoint(x: 500, y: 50),
                                  CGPoint(x: 600, y: 0),
                                  CGPoint(x: 700, y: 20),
                                  CGPoint(x: 800, y: 50),
                                  CGPoint(x: 900, y: 150),
                                  CGPoint(x: 1000, y: 200),
                                  CGPoint(x: 1100, y: 250),
                                  CGPoint(x: 1200, y: 200),
                                  CGPoint(x: 1300, y: 150),
                                  CGPoint(x: 1400, y: 100),
                                  CGPoint(x: 1500, y: 50)
                                  ],
                                               [CGPoint(x: 1500, y: 50),
                                                CGPoint(x: 1600, y: 0),
                                                CGPoint(x: 1700, y: 0),
                                                CGPoint(x: 1800, y: 50),
                                                CGPoint(x: 1900, y: 100),
                                                CGPoint(x: 2000, y: 150),
                                                CGPoint(x: 2100, y: 100),
                                                CGPoint(x: 2200, y: 50),
                                                CGPoint(x: 2300, y: 50),
                                                CGPoint(x: 2400, y: 100),
                                                CGPoint(x: 2500, y: 150),
                                                CGPoint(x: 2600, y: 100),
                                                CGPoint(x: 2700, y: 50),
                                                CGPoint(x: 2800, y: 50),
                                                CGPoint(x: 2900, y: 100),
                                                CGPoint(x: 3000, y: 50),
                                                ],
                                                    [CGPoint(x: 3000, y: 50),
                                                   CGPoint(x: 3100, y: 0),
                                                   CGPoint(x: 3200, y: 0),
                                                   CGPoint(x: 3300, y: 50),
                                                   CGPoint(x: 3400, y: 100),
                                                   CGPoint(x: 3500, y: 150),
                                                   CGPoint(x: 3600, y: 100),
                                                   CGPoint(x: 3700, y: 50),
                                                   CGPoint(x: 3800, y: 50),
                                                   CGPoint(x: 3900, y: 100),
                                                   CGPoint(x: 4000, y: 150),
                                                   CGPoint(x: 4100, y: 100),
                                                   CGPoint(x: 4200, y: 50),
                                                   CGPoint(x: 4300, y: 50),
                                                   CGPoint(x: 4400, y: 100),
                                                   CGPoint(x: 4500, y: 50),
                                                   ]]
    
    override func didMove(to view: SKView) {
        print(myCar.carBodyFrame.position);
        physicsWorld.contactDelegate = self
        border.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 10, height: 1000), center: CGPoint(x: borderXPosition, y: borderYPosition))
        border.physicsBody?.affectedByGravity = false
//        node.physicsBody?.isDynamic = false
        border.physicsBody?.collisionBitMask = 0
        border.position.x = borderXPosition
        border.position.y = borderYPosition + border.size.height/2
        
        self.backgroundColor = SKColor(red: 98/255, green: 196/255, blue: 1, alpha: 0)
        
        myCamera = SKCameraNode()
        self.camera = myCamera
       
        myCar.carBodyFrame.physicsBody?.categoryBitMask = myCar.carCategory
        myCar.carBodyFrame.physicsBody?.collisionBitMask = groundCategory
        myCar.carBodyFrame.physicsBody?.contactTestBitMask = groundCategory
        
        myCar.wheelFront.physicsBody?.categoryBitMask = myCar.carCategory
        myCar.wheelFront.physicsBody?.collisionBitMask = groundCategory
        myCar.wheelFront.physicsBody?.contactTestBitMask = groundCategory
       
        myCar.wheelRear.physicsBody?.categoryBitMask = myCar.carCategory
        myCar.wheelRear.physicsBody?.collisionBitMask = groundCategory
        myCar.wheelRear.physicsBody?.contactTestBitMask = groundCategory
        
        //Spring Front
        let slideFront = SKPhysicsJointSliding.joint(withBodyA: myCar.carBodyFrame.physicsBody!, bodyB: myCar.pinFront.physicsBody!, anchor: myCar.pinFront.position, axis: CGVector(dx: 0, dy: 1))
        let springFront = SKPhysicsJointSpring.joint(withBodyA: myCar.carBodyFrame.physicsBody!, bodyB: myCar.wheelFront.physicsBody!, anchorA: myCar.wheelFront.position, anchorB: myCar.wheelFront.position)
        
        
        springFront.damping = 0.5
        springFront.frequency = 2.7
        slideFront.shouldEnableLimits = true
        slideFront.lowerDistanceLimit = 1
        slideFront.upperDistanceLimit = myCar.carBodyFrame.position.y - myCar.carBodyFrame.size.height/2 - myCar.pinFront.position.y
        
        //Spring Rear
        let slideRear = SKPhysicsJointSliding.joint(withBodyA: myCar.carBodyFrame.physicsBody!, bodyB: myCar.pinRear.physicsBody!, anchor: myCar.pinRear.position, axis: CGVector(dx: 0, dy: 1))
        let springRear = SKPhysicsJointSpring.joint(withBodyA: myCar.carBodyFrame.physicsBody!, bodyB: myCar.wheelRear.physicsBody!, anchorA: myCar.wheelRear.position, anchorB: myCar.wheelRear.position)
        
        
        springRear.damping = 0.5
        springRear.frequency = 2.9
        slideRear.shouldEnableLimits = true
        slideRear.lowerDistanceLimit = 1
        slideRear.upperDistanceLimit = myCar.carBodyFrame.position.y - myCar.carBodyFrame.size.height/2 - myCar.pinRear.position.y
        
        
        
        
        //Front Wheel Pin
        let pinFrontWheel = SKPhysicsJointPin.joint(withBodyA: myCar.pinFront.physicsBody!, bodyB: myCar.wheelFront.physicsBody!, anchor: myCar.pinFront.position)
        
        //Rear Wheel Pin
        let pinRearWheel = SKPhysicsJointPin.joint(withBodyA: myCar.pinRear.physicsBody!, bodyB: myCar.wheelRear.physicsBody!, anchor: myCar.pinRear.position)
        
        
        self.addChild(myCar.carBodyFrame)
        
        self.addChild(myCar.pinFront)
        self.addChild(myCar.wheelFront)
        
        self.addChild(myCar.pinRear)
        self.addChild(myCar.wheelRear)
        self.addChild(border)
        
        self.addChild(myCamera!)
        
        self.physicsWorld.add(slideFront)
        self.physicsWorld.add(springFront)
        self.physicsWorld.add(pinFrontWheel)
        
        self.physicsWorld.add(slideRear)
        self.physicsWorld.add(springRear)
        self.physicsWorld.add(pinRearWheel)
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touched = true
        for touch in touches {
            location = touch.location(in:self)
        }
        
        if(location.x > myCar.carBodyFrame.position.x) {
            moveForward = true
        } else {
            moveForward = false
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        touched = true
        for touch in touches {
            location = touch.location(in:self)
        }
        
        if(location.x > myCar.carBodyFrame.position.x) {
            moveForward = true
        } else {
            moveForward = false
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touched = false
    }
    
    
    func airRotation(){
    
        if (touched) {
    
            if(moveForward) {
                if((myCar.carBodyFrame.physicsBody?.angularVelocity)! < CGFloat(0.6)) {
                    myCar.carBodyFrame.physicsBody?.applyAngularImpulse(0.1)
                }
                
            } else {
                if((myCar.carBodyFrame.physicsBody?.angularVelocity)! > CGFloat(-0.6)) {
                    myCar.carBodyFrame.physicsBody?.applyAngularImpulse(-0.1)
                }
            }
            
        }
    }
    
    // Called before each frame is rendered
    override func update(_ currentTime: TimeInterval) {
//        print(myCar.pinRear.position.x);
        
        if (touched) {
            if(moveForward) {
                if((myCar.wheelFront.physicsBody?.angularVelocity)! > CGFloat(-32.0)) {
                    myCar.wheelFront.physicsBody?.applyTorque(-1)
                }
                
                if((myCar.wheelRear.physicsBody?.angularVelocity)! > CGFloat(-32.0)) {
                    myCar.wheelRear.physicsBody?.applyTorque(-4)
                }
                
                
            }
            else {
                print(borderXPosition, myCar.pinRear.position.x);
                if((myCar.wheelRear.physicsBody?.angularVelocity)! < CGFloat(32.0)) {
                    myCar.wheelRear.physicsBody?.applyTorque(1)
                }
                if((myCar.wheelFront.physicsBody?.angularVelocity)! < CGFloat(32.0)) {
                    myCar.wheelFront.physicsBody?.applyTorque(4)
                }
            }
        }
        
        if(isOnGround == false) {
            airRotation()
        }
        
        myCamera?.position.x = myCar.carBodyFrame.position.x + myCar.carBodyFrame.size.width/2
        myCamera?.position.y = myCar.carBodyFrame.position.y
        
        if (myCar.carBodyFrame.position.x > CGFloat((myCount * 1500) - 700)) && myCount%3 == 0 {
//            print(myCount)
            
            if(myCount > 0) {
            
                groundXY.remove(at: 1)
                //is 1 and not 2 because we removed the first element from the array
                groundXY.insert(generatePoints(previousPoint: groundXY[0][14], myPoint: groundXY[0][15]), at: 1)
//                print(groundXY[1])
                borderYPosition = groundXY[2][0].y;
            }
            
//            print(groundXY[myCount%3])
            let splineShapeNode = SKShapeNode(splinePoints: &groundXY[0],
                                              count: groundXY[myCount%3].count)
            
//            let splineShapeGround = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[myCount%3].count)
//            splineShapeGround.lineWidth = 32
//            splineShapeGround.position.y = -30
//            splineShapeGround.strokeTexture = groundTexture0
//            splineShapeGround.name = "splineShapeGround"
            
//            let splineShapeGround0 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[myCount%3].count)
//            splineShapeGround0.lineWidth = 32
//            splineShapeGround0.position.y = -60
//            splineShapeGround0.strokeTexture = groundTexture0
//            splineShapeGround0.name = "splineShapeGround0"
//
//            let splineShapeGround1 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[myCount%3].count)
//            splineShapeGround1.lineWidth = 32
//            splineShapeGround1.position.y = -90
//            splineShapeGround1.strokeTexture = groundTexture1
//            splineShapeGround1.name = "splineShapeGround1"
//
//            let splineShapeGround2 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[myCount%3].count)
//            splineShapeGround2.lineWidth = 32
//            splineShapeGround2.position.y = -120
//            splineShapeGround2.strokeTexture = groundTexture2
//            splineShapeGround2.name = "splineShapeGround2"
//
//            let splineShapeGround3 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[myCount%3].count)
//            splineShapeGround3.lineWidth = 32
//            splineShapeGround3.position.y = -150
//            splineShapeGround3.strokeTexture = groundTexture3
//            splineShapeGround3.name = "splineShapeGround3"
//
//            let splineShapeGround4 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[myCount%3].count)
//            splineShapeGround4.lineWidth = 32
//            splineShapeGround4.position.y = -180
//            splineShapeGround4.strokeTexture = groundTexture4
//            splineShapeGround4.name = "splineShapeGround4"
//
//            let splineShapeGround5 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[myCount%3].count)
//            splineShapeGround5.lineWidth = 32
//            splineShapeGround5.position.y = -210
//            splineShapeGround5.strokeTexture = groundTexture5
//            splineShapeGround5.name = "splineShapeGround5"
//
//            let splineShapeGround6 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[0].count)
//            splineShapeGround6.lineWidth = 32
//            splineShapeGround6.position.y = -240
//            splineShapeGround6.strokeTexture = groundTexture6
//            splineShapeGround6.name = "splineShapeGround6"
//
//            let splineShapeGround7 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[0].count)
//            splineShapeGround7.lineWidth = 32
//            splineShapeGround7.position.y = -270
//            splineShapeGround7.strokeTexture = groundTexture7
//            splineShapeGround7.name = "splineShapeGround7"
//
//            let splineShapeGround8 = SKShapeNode(splinePoints: &groundXY[0], count: groundXY[0].count)
//            splineShapeGround8.lineWidth = 32
//            splineShapeGround8.position.y = -300
//            splineShapeGround8.strokeTexture = groundTexture8
//            splineShapeGround8.name = "splineShapeGround8"
            
            
            
            splineShapeNode.lineWidth = 32
            splineShapeNode.physicsBody = SKPhysicsBody(edgeChainFrom: splineShapeNode.path!)
            splineShapeNode.physicsBody?.isDynamic = false
            
            splineShapeNode.strokeTexture = groundTexture
            splineShapeNode.name = "splineShapeNode"
            splineShapeNode.physicsBody?.categoryBitMask = groundCategory

            self.addChild(splineShapeNode)
//            self.addChild(splineShapeGround)
//            self.addChild(splineShapeGround0)
//            self.addChild(splineShapeGround1)
//            self.addChild(splineShapeGround2)
//            self.addChild(splineShapeGround3)
//            self.addChild(splineShapeGround4)
//            self.addChild(splineShapeGround5)
//            self.addChild(splineShapeGround6)
//            self.addChild(splineShapeGround7)
//            self.addChild(splineShapeGround8)
           
            
            
            for node in self.children{
                if node.name == "splineShapeNode1"{
                    node.removeFromParent()
                    print("nod eliminat")
//                    print(node.position.x);
                    borderXPosition = CGFloat(myCount - 1) * 1500;
                    border.position.x = borderXPosition
                    border.position.y = borderYPosition + border.size.height/2
                    print("pozitia 3000", borderXPosition);
                    print("pozitia masinii ", myCar.pinRear.position.x);
                    
                }
            }
            
            
            myCount += 1
        }
        else if (myCar.carBodyFrame.position.x > CGFloat((myCount * 1500) - 700)) && myCount%3 == 1{
//            print(groundXY[myCount%3])
            
    
//                print(groundXY[0])
            groundXY.remove(at: 2)
            //is 1 and not 2 because we removed the first element from the array
            groundXY.insert(generatePoints(previousPoint: groundXY[1][14], myPoint: groundXY[1][15]), at: 2)
//            print(groundXY[2])
            
            borderYPosition = groundXY[0][0].y + border.size.height/2
            
            let splineShapeNode1 = SKShapeNode(splinePoints: &groundXY[1], count: groundXY[myCount%3].count)
            splineShapeNode1.lineWidth = 32
            splineShapeNode1.physicsBody = SKPhysicsBody(edgeChainFrom: splineShapeNode1.path!)
            splineShapeNode1.physicsBody?.isDynamic = false
            splineShapeNode1.strokeTexture = groundTexture
            splineShapeNode1.name = "splineShapeNode1"
            splineShapeNode1.physicsBody?.categoryBitMask = groundCategory
            self.addChild(splineShapeNode1)
            
            
            for node in self.children{
                if node.name == "splineShapeNode2" {
                    node.removeFromParent()
                    print("nod eliminat")
                    borderXPosition = CGFloat(myCount - 1) * 1500;
                    border.position.x = borderXPosition
                    border.position.y = borderYPosition
                    print("pozitia 4500", borderXPosition);
                    print("pozitia masinii ", myCar.pinRear.position.x);
                    
                }
            }
            
            myCount += 1
        }
        
        
        else if (myCar.carBodyFrame.position.x > CGFloat((myCount * 1500) - 700))  && myCount%3 == 2{
//            print(groundXY[myCount%3])
//            let splineShapeNode2 = SKShapeNode(splinePoints: &groundXY[2], count: groundXY[myCount%3].count)
//            print(groundXY[0])
            groundXY.remove(at: 0)
            //is 1 and not 2 because we removed the first element from the array
            groundXY.insert(generatePoints(previousPoint: groundXY[1][14], myPoint: groundXY[1][15]), at: 0)
//            print(groundXY[0])
            //
            borderYPosition = groundXY[1][0].y + border.size.height/2
            let splineShapeNode2 = SKShapeNode(splinePoints: &groundXY[2], count: groundXY[myCount%3].count)
            
            splineShapeNode2.lineWidth = 32
            splineShapeNode2.physicsBody = SKPhysicsBody(edgeChainFrom: splineShapeNode2.path!)
            splineShapeNode2.physicsBody?.isDynamic = false
            splineShapeNode2.strokeTexture = groundTexture
            splineShapeNode2.name = "splineShapeNode2"
            splineShapeNode2.physicsBody?.categoryBitMask = groundCategory
            self.addChild(splineShapeNode2)
            
            for node in self.children{
               
                switch node.name{
                case "splineShapeNode":
                    node.removeFromParent()
                    borderXPosition = CGFloat(myCount - 1) * 1500;
                    border.position.x = borderXPosition
                    border.position.y = borderYPosition
                    print("pozitia 1500", borderXPosition);
                    print("pozitia masinii ", myCar.pinRear.position.x);
                    
                case "splineShapeGround":
                    node.removeFromParent()
                case "splineShapeGround0":
                    node.removeFromParent()
                
                case "splineShapeGround1":
                    node.removeFromParent()
                
                case "splineShapeGround2":
                    node.removeFromParent()
                
                case "splineShapeGround3":
                    node.removeFromParent()
                
                case "splineShapeGround4":
                    node.removeFromParent()
                
                case "splineShapeGround5":
                    node.removeFromParent()
                
                case "splineShapeGround6":
                    node.removeFromParent()
                
                case "splineShapeGround7":
                    node.removeFromParent()
                    
                case "splineShapeGround8":
                    node.removeFromParent()
                    
                default:
                   break
                }
            }
            myCount += 1
            
        }
    }
}

func generatePoints(previousPoint: CGPoint, myPoint: CGPoint)->[CGPoint] {
    var pointsArray: [CGPoint] = [myPoint]
    let x = myPoint.x;
    var y = myPoint.y;
    let slope = myPoint.y - previousPoint.y;
    for x1 : CGFloat in stride(from: 100, to: 1600, by: 100.0) {
        if x1 == 100 {
            pointsArray.append(CGPoint(x: x1+x, y: y+slope))
        }
        else {
            y += CGFloat.random(in: -50.0...50.0)
            pointsArray.append(CGPoint(x: x1+x, y: y))
        }
    }
    return pointsArray
}

extension GameScene: SKPhysicsContactDelegate{
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let contactMask = contact.bodyA.categoryBitMask & contact.bodyB.categoryBitMask
        
        switch contactMask {
        case myCar.carCategory & groundCategory:
            isOnGround = true
//            print("Masina este pe pamant")
        default:
            break
        }
        
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        
        let contactMask = contact.bodyA.categoryBitMask & contact.bodyB.categoryBitMask
        
        switch contactMask {
        case myCar.carCategory & groundCategory:
            
            isOnGround = false
//            print("Masina este in saritura")
        default:
            break
        }
    }
}
